 class Pais {
    constructor(idPais, nombre) {
        this.idPais = idPais;
        this.nombre = nombre;
    }
}

module.exports = {
    Pais: Pais
};