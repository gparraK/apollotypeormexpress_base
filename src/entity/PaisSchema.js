
const EntitySchema = require("typeorm").EntitySchema; // import {EntitySchema} from "typeorm";
const Pais = require("../model/Pais").Pais; // import {Category} from "../model/Category";

module.exports = new EntitySchema({
    name: "Pais",
    target: Pais,
    columns: {
        idPais: {
            primary: true,
            type: "int",
            generated: true
        },
        nombre: {
            type: "varchar"
        }
    }
});
