const express = require('express');
const { ApolloServer, gql } = require('apollo-server-express');

const typeorm = require("typeorm"); // import * as typeorm from "typeorm";
const Pais = require("./src/model/Pais").Pais; 
const {schema} = require("./schema");



typeorm.createConnection({
    type: "mssql",
    host: "localhost",
    port: 1433,
    username: "multidoctores",
    password: "hambre!123",
    database: "multidoctores_local",
    synchronize: false,
    logging: false,
    entities: [
        require("./src/entity/PaisSchema")
    ]
}).then(function (connection) {


  const app = express();


  const resolvers = {
  
    Query: {
  
  
      hellos: () => 'Hola hola',
  
      paises: () => {
        return connection
            .manager
            .find(Pais)
            .then((paises) => {
    
                return paises
            });}
  
    }
  
    };

  
  
  const server = new ApolloServer({
    typeDefs: schema,
    resolvers,
  });
  
  server.applyMiddleware({ app, path: '/graphql' });
  
  app.listen({ port: 8000 }, () => {
    console.log('Apollo Server on http://localhost:8000/graphql');
  });


}).catch(function(error) {
    console.log("Error: ", error);
});


