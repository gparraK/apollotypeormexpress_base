const { gql } = require('apollo-server-express');



const schema = gql`
  
type Pais {
  idPais: Int
  nombre: String
}



# This is for marking an operation or a mutation
type Query {
  paises: [Pais]
hellos: String
}


`;

module.exports.schema = schema;